#!/bin/bash
cat /etc/passwd | grep -v "\#" | cut -d ":" -f1 |  awk '!(NR%2)' | rev | sort --reverse | awk "NR >= $FT_LINE1 && NR <= $FT_LINE2" | tr "\n" "," | sed 's/, */, /g' | sed 's/,.$/\./' | xargs echo -n
