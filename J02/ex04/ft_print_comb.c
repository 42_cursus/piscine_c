/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 16:53:20 by adelhom           #+#    #+#             */
/*   Updated: 2016/08/31 21:50:11 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_print_comb(void)
{
	char c1;
	char c2;
	char c3;

	c1 = '0' - 1;
	while (c1++ < '9')
	{
		c2 = c1;
		while (c2++ < '9')
		{
			c3 = c2;
			while (c3++ < '9')
			{
				ft_putchar(c1);
				ft_putchar(c2);
				ft_putchar(c3);
				if (c1 + c3 != '7' + '9')
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
			}
		}
	}
}
