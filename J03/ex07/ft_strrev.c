/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/01 22:39:09 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/02 00:27:44 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrev(char *str)
{
	int		i;
	int		size;
	char	temp;

	i = 0;
	size = 0;
	while (str[size] != '\0')
		size++;
	size = size - 1;
	while (size > i)
	{
		temp = str[size];
		str[size] = str[i];
		str[i] = temp;
		size--;
		i++;
	}
	return (str);
}
