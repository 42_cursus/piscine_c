gcc -Wextra -Wall -Werror -c ft_*.c
ar rc libft.a ft_*.o
find . -name "*.o" -delete
ranlib libft.a
