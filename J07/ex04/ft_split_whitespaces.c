/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 19:36:53 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/08 19:54:06 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#define IS_SEPARATOR(x) (x==' ' || x=='\n' || x=='\t')

char	**ft_alloc_table(char **table, char *str)
{
	int		i;
	int		counts;

	i = 0;
	counts = 0;
	while (IS_SEPARATOR(str[i]))
		i++;
	while (str[i] != '\0')
	{
		i++;
		if (IS_SEPARATOR(str[i]))
		{
			counts++;
			while (IS_SEPARATOR(str[i]))
				i++;
		}
	}
	if (!IS_SEPARATOR(str[i - 1]))
		counts++;
	table = (char**)malloc(sizeof(*table) * (counts + 1));
	table[counts] = 0x0;
	return (table);
}

char	**ft_alloc_str(char **table, char *str)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	j = 0;
	k = 0;
	table = ft_alloc_table(table, str);
	while (IS_SEPARATOR(str[i]))
		i++;
	while (str[i] != '\0')
	{
		i++;
		if (IS_SEPARATOR(str[i]) || str[i] == '\0')
		{
			table[j] = (char*)malloc(sizeof(**table) * (k + 1));
			j++;
			k = 0;
			while (IS_SEPARATOR(str[i]))
				i++;
		}
		k++;
	}
	return (table);
}

char	**ft_put_table(char **table, char *str)
{
	int	i;
	int	j;
	int	k;

	i = 0;
	j = 0;
	k = 0;
	table = ft_alloc_str(table, str);
	while (IS_SEPARATOR(str[i]))
		i++;
	while (str[i] != '\0')
	{
		table[j][k] = str[i];
		i++;
		k++;
		if (IS_SEPARATOR(str[i]))
		{
			table[j][k] = '\0';
			j++;
			k = 0;
			while (IS_SEPARATOR(str[i]))
				i++;
		}
	}
	return (table);
}

char	**ft_split_whitespaces(char *str)
{
	char **table;

	table = NULL;
	table = ft_put_table(table, str);
	return (table);
}
