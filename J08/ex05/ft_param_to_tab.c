/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 06:01:48 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/09 16:01:04 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"
#include <stdio.h>
#include <stdlib.h>

char				*ft_strdup(char *src)
{
	int		i;
	int		size;
	char	*dup;

	i = 0;
	size = 0;
	while (src[size] != '\0')
		size++;
	dup = (char*)malloc(sizeof(*dup) * (size + 1));
	while (src[i] != '\0')
	{
		dup[i] = src[i];
		i++;
	}
	dup[i] = '\0';
	return (dup);
}

int					ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

struct s_stock_par	*ft_param_to_tab(int ac, char **av)
{
	int			i;
	t_stock_par	*result;
	t_stock_par *table;

	i = 0;
	result = (t_stock_par *)malloc(sizeof(t_stock_par) * (ac + 2));
	table = result;
	while (i < ac)
	{
		table->size_param = ft_strlen(av[i]);
		table->str = av[i];
		table->copy = ft_strdup(av[i]);
		table->tab = ft_split_whitespaces(av[i]);
		table++;
		i++;
	}
	table->str = 0;
	return (result);
}
