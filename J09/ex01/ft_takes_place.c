/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 21:14:54 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/09 00:02:59 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_takes_place(int hour)
{
	int i;

	i = hour;
	if (i >= 12)
		i = hour % 12;
	if (i == 0 && i == 12)
		i = 12;
	printf("THE FOLLOWING TAKES PLACE BETWEEN");
	if (hour < 23)
		printf("%d.00 P.M. AND %d.00 P.M.\n", i, (i + 1) % 12);
	if (hour < 11)
		printf("%d.00 A.M. AND %d.00 A.M.\n", i, (i + 1));
	if (hour == 0)
		printf("12.00 A.M. AND 1.00 A.M.\n");
	if (hour == 11)
		printf("12.00 A.M. AND 12.00 P.M.\n");
	if (hour == 24)
		printf("12.00 A.M. AND 1.00 A.M.\n");
	if (hour == 23)
		printf("11.00 P.M. AND 12.00 A.M.\n");
}
