/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_active_bits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: redi-san <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/09 10:42:26 by redi-san          #+#    #+#             */
/*   Updated: 2016/09/09 10:44:58 by redi-san         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_active_bits(int value)
{
	int compteur;

	compteur = 0;
	while (value > 0)
	{
		value >>= 1;
		compteur++;
	}
	return (compteur);
}
