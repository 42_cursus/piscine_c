/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculation.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 20:08:54 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/12 23:13:49 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CALCULATION_H
# define CALCULATION_H

int			ft_add(int a, int b);
int			ft_sou(int a, int b);
int			ft_mul(int a, int b);
int			ft_div(int a, int b);
int			ft_mod(int a, int b);
typedef int	(*t_ft)(int, int);
t_ft		g_ft[] = {ft_add, ft_sou, ft_mul, ft_div, ft_mod};

#endif
