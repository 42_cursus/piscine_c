/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printer.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 20:19:59 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/12 22:02:47 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTER_H
# define PRINTER_H
# define DIV_ZERO "Stop : division by zero\n"
# define MOD_ZERO "Stop : modulo by zero\n"

# include <unistd.h>

void	ft_putchar(char c);
void	ft_putnbr(int nbr);
void	ft_putstr(char *str);
int		ft_atoi(char *str);

#endif
