/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 21:38:52 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/12 23:24:57 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/calculation.h"
#include "../includes/printer.h"

int	check_operator(char *operator)
{
	char	*operator_list;
	int		i;

	operator_list = "+-*/%";
	i = 0;
	while (operator[i])
		i++;
	if (i != 1)
		return (-1);
	i = 0;
	while (i < 5)
	{
		if (*operator == operator_list[i])
			return (i);
		i++;
	}
	return (-1);
}

int	check_and_return(int operator, int a, int b)
{
	if (operator == 3 && !b)
	{
		ft_putstr(DIV_ZERO);
		return (1);
	}
	if (operator == 4 && !b)
	{
		ft_putstr(MOD_ZERO);
		return (1);
	}
	if (operator >= 0)
	{
		ft_putnbr(g_ft[operator](a, b));
		ft_putchar('\n');
		return (1);
	}
	return (0);
}

int	main(int argc, char **argv)
{
	int a;
	int b;
	int operator;

	if (argc != 4)
		return (0);
	operator = check_operator(argv[2]);
	a = ft_atoi(argv[1]);
	b = ft_atoi(argv[3]);
	if (check_and_return(operator, a, b))
		return (0);
	ft_putstr("0\n");
	return (0);
}
