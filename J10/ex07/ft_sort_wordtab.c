/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_wordtab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/12 23:50:22 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/13 10:24:29 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(char *s1, char *s2)
{
	int i;
	int compare;

	i = 0;
	compare = 0;
	while (1 == 1)
	{
		compare = s1[i] - s2[i];
		if (s1[i] == '\0' && s2[i] == '\0')
			return (compare);
		else if (s1[i] == s2[i])
			i++;
		else
			return (compare);
	}
}

void	ft_sort_wordtab(char **tab)
{
	int		i;
	int		j;
	char	*temp;

	i = 2;
	while (tab[i])
	{
		j = i;
		while (j > 1)
		{
			if (ft_strcmp(*(tab + j), *(tab + j - 1)) < 0)
			{
				temp = *(tab + j);
				*(tab + j) = *(tab + j - 1);
				*(tab + j - 1) = temp;
			}
			j--;
		}
		i++;
	}
}
