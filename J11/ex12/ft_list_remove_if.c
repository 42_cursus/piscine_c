/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 10:28:58 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/14 10:49:41 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)())
{
	t_list	*list;
	t_list	*tmp;
	t_list	*front;

	list = *begin_list;
	tmp = NULL;
	front = NULL;
	while (list)
	{
		if ((*cmp)(list->data, data_ref) == 0)
		{
			if (list == *begin_list)
				*begin_list = list->next;
			else
				front->next = list->next;
			tmp = list;
			list = list->next;
			free(tmp);
		}
		else
		{
			front = list;
			list = list->next;
		}
	}
}
