/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 12:18:30 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/14 15:42:45 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_swap_list(t_list **list)
{
	t_list	*tmp;

	tmp = (*list)->next->next;
	(*list)->next->next = (*list);
	(*list)->next = tmp;
}

void	ft_list_sort(t_list **begin_list, int (*cmp)())
{
	t_list	*list;
	t_list	*last;

	if (!*begin_list)
		return ;
	list = *begin_list;
	while (list->next)
	{
		if ((*cmp)(list->data, list->next->data) > 0)
		{
			if (current != *begin_list)
				last->next = list->next;
			else
				*begin_list = list->next;
			ft_swap_list(&list);
			list = *begin_list;
		}
		else
		{
			last = list;
			list = list->next;
		}
	}
}
