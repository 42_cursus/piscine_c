/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:43:25 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 20:43:47 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "../includes/ft_printer.h"

#define BUFFER_SIZE 10

void	ft_display_file(char *file)
{
	int		fd;
	int		ret;
	char	buffer[BUFFER_SIZE + 1];

	fd = open(file, O_RDONLY);
	if (fd == -1)
		return ;
	while ((ret = read(fd, buffer, BUFFER_SIZE)))
	{
		buffer[ret] = '\0';
		write(1, buffer, ret);
	}
	close(fd);
}

int		main(int argc, char **argv)
{
	if (argc == 1)
	{
		ft_putstr(FILE_ERROR, 2);
		return (0);
	}
	if (argc > 2)
	{
		ft_putstr(ARGUMENT_ERROR, 2);
		return (0);
	}
	ft_display_file(argv[1]);
	return (0);
}
