/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:39:13 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/14 23:25:30 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c, int flag)
{
	write(flag, &c, 1);
}

void	ft_putstr(char *str, int flag)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i], flag);
		i++;
	}
}
