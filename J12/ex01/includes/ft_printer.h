/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printer.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:55:15 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/14 21:12:52 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTER_H
# define FT_PRINTER_H
# define FILE_ERROR "File name missing.\n"
# define ARGUMENT_ERROR "Too many arguments.\n"

void	ft_putchar(char c, int flag);
void	ft_putstr(char *str, int flag);

#endif
