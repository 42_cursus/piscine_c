/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 15:27:26 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 17:58:15 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_UTILS_H
# define FT_UTILS_H
# define BUFFER_SIZE 10
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <errno.h>

int		ft_get_file_size(char *file);
int		ft_atoi(char *str);
int		ft_is_number(char *str);

#endif
