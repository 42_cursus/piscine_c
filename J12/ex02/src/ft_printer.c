/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:39:13 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 18:11:33 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c, int flag)
{
	write(flag, &c, 1);
}

void	ft_putstr(char *str, int flag)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i], flag);
		i++;
	}
}

void	ft_putnbr(int nbr, int flag)
{
	if (nbr == -2147483648)
	{
		ft_putchar('-', flag);
		ft_putnbr(2, flag);
		ft_putnbr(147483648, flag);
		return ;
	}
	else if (nbr < 0)
	{
		ft_putchar('-', flag);
		nbr = -nbr;
	}
	if (nbr >= 10)
	{
		ft_putnbr(nbr / 10, flag);
		ft_putnbr(nbr % 10, flag);
	}
	else
		ft_putchar(nbr + '0', flag);
}
