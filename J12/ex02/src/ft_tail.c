/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/15 15:24:00 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 20:45:48 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printer.h"
#include "../includes/ft_utils.h"

int		ft_wrong_arg(int argc, char **argv)
{
	if (argc < 3)
	{
		ft_putchar('\n', 0);
		return (1);
	}
	if (!ft_is_number(argv[2]))
	{
		ft_putstr(OFFSET_ERROR, 2);
		ft_putstr(argv[2], 2);
		ft_putchar('\n', 2);
		return (1);
	}
	return (0);
}

int		ft_wrong_file(char *file)
{
	int	fd;

	fd = open(file, O_RDONLY);
	if (fd == -1)
	{
		ft_putstr("tail: ", 2);
		ft_putstr(file, 2);
		ft_putstr(FILE_ERROR, 2);
		return (1);
	}
	close(fd);
	return (0);
}

void	ft_several_file(char *file, int argc)
{
	if (argc > 4)
	{
		ft_putstr("==> ", 1);
		ft_putstr(file, 1);
		ft_putstr(" <==\n", 1);
	}
}

void	ft_tail(char *file, int argc, int size)
{
	int		fd;
	int		ret;
	int		size_file;
	char	buffer[BUFFER_SIZE + 1];
	char	*tmp_buffer;

	if (!ft_wrong_file(file))
	{
		size_file = ft_get_file_size(file);
		fd = open(file, O_RDONLY);
		if (size < size_file)
		{
			tmp_buffer = (char*)malloc(sizeof(char) * (size_file - size + 1));
			read(fd, tmp_buffer, size_file - size);
			free(tmp_buffer);
		}
		ft_several_file(file, argc);
		while ((ret = read(fd, buffer, BUFFER_SIZE)) > 0)
		{
			buffer[ret] = '\0';
			ft_putstr(buffer, 1);
		}
	}
}

int		main(int argc, char **argv)
{
	int i;

	if (ft_wrong_arg(argc, argv))
		return (0);
	i = 2;
	while (i++ < argc - 1)
	{
		ft_tail(argv[i], argc, ft_atoi(argv[2]));
		if (i != argc - 1)
			ft_putchar('\n', 1);
	}
	return (errno);
}
