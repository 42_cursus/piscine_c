/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_search_item.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/16 13:11:00 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/16 15:22:40 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_btree.h"

void	*btree_search_item(t_btree *root,
						void *data_ref,
						int (*cmpf)(void *, void *))
{
	void	*result;

	result = NULL;
	if (root)
	{
		result = t_btree_search_item(root->left, data_ref, cmpf);
		if (!result)
		{
			if ((*cmpf)(root->item, data_ref) == 0)
				result = root->item;
			else
				result = t_btree_search_item(root->right, data_ref, cmpf);
		}
	}
	return (result);
}
